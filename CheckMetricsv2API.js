var ctx = {
    data: {
        MergeReport: false,
        SendEmail: false
    }
};

module.exports = function(context, cb) {
    var cfg = context.data;

    //------ Mail settings ------------------
    var eml_recepient; //Can be comma-separated list
    var eml_copy_to; //Can be comma-separated list
    var eml_from;
    var eml_sender;

    var eml_notify_title = '[Thrive!] [%s] %u Traffic channels outsiders detected! (%s)';
    var eml_merge_title = '[Thrive!] %u Daily ROI outsiders detected (%s)';
    var eml_merge_h2 = 'Big list of problems for last time %s :';
    var eml_notify_h2 = '[%s] Something went wrong today (%s) with:';

    //------ Mail settings ------------------
    var mailTemplate = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head>'+
        '<meta property="og:title" content="%s" />'+
        '<title>%s</title>'+
        '<style type="text/css">'+
        'table, td, th{'+
            'border: 1px solid black;'+
            'border-collapse: collapse;'+
            'padding: 2px;'+
            '}'+
        '</style>'+
    '</head>'+
        '<body>'+
            '<h2>%s</h2>%s'+
        '</body>'+
    '</html>';

    /* //Use this requres on the stamplay side to use strict versions of needed components
    var request = require('request-promise@1.0.2');
    var promise = require('bluebird@2.9.26');
    var htmlToText = require('html-to-text@1.5.0');
    var crypto = require('crypto');
    var _ = require('underscore@1.8.3');
    var moment = require('moment@2.10.3');
    var sprintf = require('sprintf-js@1.0.2').sprintf;
    */

    var request = require('request-promise');
    var promise = require('bluebird');
    var htmlToText = require('html-to-text');
    var crypto = require('crypto');
    var _ = require('underscore');
    var moment = require('moment');
    var sprintf = require('sprintf-js').sprintf;

    //Host name for using API
    var rootHost;
    //API path extension for calling methods
    var apiRoot = '/ajax';
    //Traffic sources fetch endpoint
    var apiGetSources = '/sources/get';
    //Getting metrics for variables by date range
    var apiGetVariables = '/otherVariables/getMetrics';

    //Name of the variable to group when getting metrics
    var groupVariable = 'v0';
    //Timezone for building reports
    var timeZone;
    var timeZoneShift;

    //----------------Security critical-----------------
    //API Settings
    var apiKey;
    var apiSecret;
    var apiParam;

    //Stamplay API secrets
    var stamAPIAppID = 'thrive';
    var stamAPIAppSecret = '8084091ecd27a466ae7dbd0d34a152c7b2fa6d01c1b11438d9ee69e11aee4f44';
    //--------------------------------------------------

    //Stampay application url
    var stamUrl = 'http://'+stamAPIAppID+'.stamplayapp.com/api';
    var stamObject = '/cobject/v1/outsiders';
    var stamSettings = '/cobject/v1/settings';
    var stamResponsible = '/cobject/v1/responsible';
    var stamMail = '/email/v1/send';

    //---------------Settings---------------------------
    var isMergeReport = !!cfg.MergeReport;
    var isSendEmail = !!cfg.SendEmail;

    var ROILimit;
    var LossLimit;
    var HideClicks;
    var mergeReportROILossLimit;
    var mergeHours;
    var marginLevel;
    //--------------------------------------------------

    //Get authentification hash for API calls
    function makeApiHash(key, secret, offset) {
        offset = offset || 0;
        if(typeof key !== 'string' || key.length != 32) {
            return false;
        }
        if(typeof secret !== 'string' || secret.length != 16) {
            return false;
        }
        var now = Math.floor(new Date() / 1000) + (offset * 60);
        // hash changes every 60 seconds
        var date = Math.round(Math.ceil((now / 60)) * 60);

        var md5 = crypto.createHash('md5').update(key + secret + date).digest("hex");
        var sha256 = crypto.createHash('sha256').update(md5).digest("hex");
        return sha256;
    }

    //Perform Thrive API call
    function apiCall(uri, requestParams){
        var retCall = null;
        var apiHash = makeApiHash(apiKey, apiSecret);
        if (apiHash){
            var requestOptions = {
                uri: uri,
                method: 'POST',
                json: true
            };
            requestParams = requestParams || {};
            requestParams[apiParam] = apiHash;
            if (requestParams){
                requestOptions.form = requestParams;
            }
            retCall = request(requestOptions);
        } else {
            throw new Error('Cant call API, api hash cannot be constructed : '+JSON.stringify(requestParams));
        }
        return retCall;
    }

    //Get camaigns and corresponding traffic sources
    function getChannelsWithVaiables(){
        return apiCall(rootHost+apiRoot+apiGetSources).then(
            function(payload){
                var sources = [];
                var sourceList = [];
                if (payload.error){
                    //console.log(payload);
                    throw new Error('Cant get trafffic sources '+payload.message);
                } else {
                    for (var i = 0; i<payload.data.length; i++){
                        var source = payload.data[i];
                        var sourceInfo = _.pick(source, 'sourceId', 'abbr', 'name');
                        //console.log(sourceInfo);
                        sourceList.push(sourceInfo);
                        for (var j = 0; j<source.camps.length; j++){
                            var camp = source.camps[j];
                            camp.source = sourceInfo;
                            sources.push(camp);
                        }
                    }
                }
                return {camps: sources, sources: sourceList};
            });
    }

    //Get metrics for concrete camaign
    function getMetricsForCamaign(camaignId, fromDateString, toDateString){
        //console.log('from '+ fromDateString + ' to ' +toDateString);
        return apiCall(rootHost+apiRoot+apiGetVariables, {
            camps: [camaignId],
            key: groupVariable,
            range: {
                from: fromDateString,
                to: toDateString || fromDateString,
                timezone: timeZone
            }
        });
    }

    //Send email from stamplay
    function sendMail(title, body, email){
        //console.log(title);
        //console.log(body);
        return request({
            url: stamUrl+stamMail,
            method: 'POST',
            json: {
                from: eml_from,
                to: email,
                bcc: eml_copy_to,
                fromname: eml_sender,
                subject: title,
                body: body
            },
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            }
        });
    }

    //Add responsible record for setup
    function addRowToResponsible(row){
        return request({
            url: stamUrl+stamResponsible,
            method: 'POST',
            json: {
                source_id: row.sourceId,
                source_abbr: row.abbr,
                email: ''
            },
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            }
        });
    }

    //Adding record to storage about outdider
    function addRowToStorage(row){
        return request({
            url: stamUrl+stamObject,
            method: 'POST',
            json: {
                clicks: row.clicks,
                cost: row.cost,
                cpc: row.cpc,
                ecpc: row.eCPC,
                key: row.key,
                plus_clicks: row.plus_clicks,
                profit: row.profit,
                roi: row.roi,
                target: row.value,
                r_date: row.r_date,
                source_id: row.source_id,
                source_abbr: row.source_abbr,
                comp_id: row.comp_id,
                comp_name: row.comp_name
            },
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            }
        });
    }

    //Update outsider info
    function updateRowInStorage(id, row){
        return request({
            url: stamUrl+stamObject+'/'+id,
            method: 'PUT',
            json: {
                clicks: row.clicks,
                cost: row.cost,
                cpc: row.cpc,
                ecpc: row.eCPC,
                key: row.key,
                plus_clicks: row.plus_clicks,
                profit: row.profit,
                roi: row.roi,
                target: row.value,
                r_date: row.r_date,
                source_id: row.source_id,
                source_abbr: row.source_abbr,
                comp_id: row.comp_id,
                comp_name: row.comp_name

            },
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            }
        });
    }

    function getResponsible(){
        return request({
            url: stamUrl+stamResponsible,
            qs: {
                select: "source_id, email"
            },
            method: 'GET',
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            },
            json: true
        });
    }

    function getSettings(){
        return request({
            url: stamUrl+stamSettings,
            qs: {
                select: "name,value"
            },
            method: 'GET',
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            },
            json: true
        });
    }

    //Get already saved list of outsiders
    function getOutsiders(keys, dateString){
        var whereClousure = {
            "$and":[
                {"key": {"$in": keys}},
                { "r_date": {"$eq": dateString}}
            ]};

        return request({
            url: stamUrl+stamObject,
            method: 'GET',
            qs: {
                where: JSON.stringify(whereClousure)
            },
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            },
            json: true
        });
    }

    //Extend campaign data with metrincs for needed period
    function getCampaignsWithMetrics(sources, fromDateString, filter, toDateString){
        var campaignsWithMetrics = [];
        if (sources) {
            //console.log(sources);
            var campaignsMetricsRq = [];
            for (var i=0; i<sources.length; i++){
                var campaign = sources[i];
                var mutator = {
                    campaign: campaign,
                    getMetrics: function(){
                        var self = this;
                        return getMetricsForCamaign(self.campaign.campId, fromDateString, toDateString).then(
                            function(metrics){
                            //console.log(metrics);
                            //Filtering metrics with our criteria
                            var currentRowsForReport = [];
                            for (var i = 0; i<metrics.length; i++){
                                var row = metrics[i];
                                row.eCPC = row.rev/row.clicks;
                                row.tCPC = row.eCPC - row.eCPC * marginLevel;
                                row.tCPC  = row.tCPC>0 ? row.tCPC: 0;
                                /*
                                if (row.rev>0){
                                    console.log('eCPC = ' + row.eCPC + ' margin = '+ marginLevel +' tCPC = '+row.tCPC);
                                    console.log('Revenue = ' + row.rev + ' Spendings when eCPC is satisfied = ' + (row.eCPC*row.clicks) + ' Difference = ' +(row.eCPC*row.clicks - row.rev));
                                    console.log('Revenue = ' + row.rev + ' Spendings when tCPC is satisfied = ' + (row.tCPC*row.clicks) + ' Difference = ' +(row.tCPC*row.clicks - row.rev)+ ' Its = '+(row.tCPC*row.clicks - row.rev)/row.rev*100 + '%');
                                }*/
                                if (filter(row)){
                                    currentRowsForReport.push(row);
                                }
                            }
                            self.campaign.metrics =  currentRowsForReport;
                            //console.log(self.campaign);
                            return self.campaign;
                        });
                    }
                };
                //Collecting async calls in collection to aggregate data later
                campaignsMetricsRq.push(mutator.getMetrics());
                campaignsWithMetrics = promise.all(campaignsMetricsRq);
            }
        }
        else {
            throw  new Error('There is no campaigns in account');
        }
        return campaignsWithMetrics;
    }

    //Get resulting data for notify report
    function processNotifyReport(camps, todayString){
        //console.log(camps);

        var result = [];

        var cmpWithMetrics = _.filter(camps, function(cmp) {return cmp.metrics.length;});
        var keys =
            _.flatten(
                _.map(cmpWithMetrics, function(cmp){
                    return _.map(cmp.metrics, function(mtr){
                        return cmp.source.sourceId+'_'+cmp.campId+'_'+mtr.value;
                    });
                })
            );
        if (cmpWithMetrics.length){
            result = getOutsiders(keys, todayString).then(function(savedOutsiders){
                var storageOps = [];
                var rowsForReport = [];
                //console.log(cmpWithMetrics);
                //console.log(savedOutsiders);
                savedOutsiders = savedOutsiders.data;
                for (var i = 0; i<cmpWithMetrics.length; i++){
                    var curComp = cmpWithMetrics[i];
                    for(var j = 0; j<curComp.metrics.length; j++){
                        var newItem = curComp.metrics[j];

                        newItem.r_date = todayString;
                        newItem.comp_id = curComp.campId;
                        newItem.source_id = curComp.source.sourceId;
                        newItem.source_abbr = curComp.source.abbr;
                        newItem.comp_name = curComp.name;
                        newItem.key =  newItem.source_id+'_'+newItem.comp_id+'_'+newItem.value;

                        var savedItem = _.findWhere(savedOutsiders, {key:  newItem.key});

                        newItem.plus_clicks = savedItem?newItem.clicks-savedItem.clicks:0;

                        //console.log(savedItem);

                        if (savedItem){ //Maybe we need to update saved one
                            if (newItem.plus_clicks>HideClicks){ //Still working/clicking and wasting money
                                rowsForReport.push(newItem);
                                if (isSendEmail){
                                    storageOps.push(updateRowInStorage(savedItem._id, newItem));
                                }
                            }
                        } else {
                            rowsForReport.push(newItem);
                            if (isSendEmail){
                                storageOps.push(addRowToStorage(newItem));
                            }
                        }
                    }
                }
                return promise.all(storageOps).then(function() {return rowsForReport;});
            });
        }
        return result;
    }

    //Extend report data with emails to who them should go
    function extendWithResponsible(reportData, sources){
        return getResponsible().then(function(resp){
                //console.log(resp);
                var respObj = _.object(_.pluck(resp.data, 'source_id'), _.pluck(resp.data, 'email'));
                var saveOps = [];
                var result = reportData;
                //console.log(respObj);
                for(var i =0; i<sources.length; i++){
                    if (respObj[''+sources[i].sourceId]===undefined){
                        //Save source as unassigned
                        //console.log(sources[i]);
                        saveOps.push(addRowToResponsible(sources[i]));
                    }
                }
                //console.log(respObj);
                for (i = 0; i<reportData.length; i++){
                    //console.log(reportData[i]);
                    reportData[i].email  = respObj[reportData[i].source_id] || eml_recepient;
                }
                if (saveOps){
                    result = promise.all(saveOps).then(function(){ return reportData; });
                }
                return result;
            });
    }

    //Create email bodies and send them
    function sendFormattedEmail(formattedData){
        var result = formattedData;
        if (formattedData.numberOfRows){
            var title;
            var h2;
            var body;
            if (formattedData.isMerge){
               title = sprintf(eml_merge_title, formattedData.numberOfRows, formattedData.today);
               h2 = sprintf(eml_merge_h2, formattedData.today);
               body = sprintf(mailTemplate, title, title, h2, formattedData.dataHtml);
            } else{
               var sourceName = formattedData.dataObject[0].source_abbr;
               title = sprintf(eml_notify_title, sourceName, formattedData.numberOfRows, formattedData.today);
               h2 = sprintf(eml_notify_h2, sourceName, formattedData.today);
               body = sprintf(mailTemplate, title, title, h2, formattedData.dataHtml);
               //console.log(title);
            }
            //console.log(formattedData.email);
            if (isSendEmail){
                result = sendMail(title, body, formattedData.email).then(function(){return formattedData;});
            }
        }
        return result;
    }

    //Creating formatted output object for stamplay
    function formatNotifyReport(rows, todayString){
        //console.log(rowsForReport);
        //Target, total spend today, loss today, ROI today, current CPC, effective CPC
        var mailBody = '';
        //console.log(rowsForReport);



        var segmentedReports = _.groupBy(rows, function(row){ return row.email; });
        var reportQueue = [];
        for (var email in segmentedReports){
            //Ordering report by Profit
            var rowsForReport = _.sortBy(segmentedReports[email], function(row) {return row.profit;});

            if (rowsForReport.length){
                mailBody = '<table style="border: 1px solid black; border-collapse: collapse;" id="result"><tr><th>SRC</th><th>CMP</th><th>TGT</th><th>SPND</th><th>PFT</th><th>ROI</th><th>CPC</th><th>eCPC</th><th>tCPC</th><th>CLK+</th></tr>';
                for (var rowIndex = 0; rowIndex<rowsForReport.length; rowIndex++){
                    var row = rowsForReport[rowIndex];
                    //traffic source, target, total spend today, loss today, ROI today, current CPC, effective CPC
                    mailBody+='<tr><td align="left">'+row.source_abbr+'</td><td align="left">'+row.comp_name+'</td><td align="left">'+row.value+'</td><td align="center">'+htmlDigit(row.cost)+'$</td><td align="center">'+htmlDigit(row.profit)+'$</td><td align="center">'+htmlDigit(+row.roi*100)+'%</td><td align="center">'+htmlDigit(row.cpc)+'$</td><td align="center">'+htmlDigit(row.eCPC)+'$</td><td align="center">'+htmlDigit(row.tCPC)+'$</td><td align="center">+'+row.plus_clicks+'clk</td></tr>';
                }
                mailBody+='</table>';
                //console.log(mailBody);
            }
            reportQueue.push({
                email: email,
                dataObject: rowsForReport,
                dataHtml: mailBody,
                dataText: htmlToText.fromString(mailBody, {tables: ['#result']}),
                numberOfRows: rowsForReport.length,
                isMerge: 0,
                today: todayString
            });
        }

        return reportQueue;
    }

    //Format report
    function formatMergeReport(camps, fromString, toString){
        var rowsForReport =
            _.filter(
                _.flatten(
                    _.map(
                        _.filter(camps, function(camp) {return camp.metrics.length; }),
                        function(camp){
                            var metrics = camp.metrics;
                            _.each(metrics, function(metric) {
                               metric.source_abbr = camp.source.abbr;
                               metric.comp_name = camp.name;
                            });
                            return metrics;
                    })
                ),
                function(row){
                    return row.clicks>0 && row.cost>0; //We are not interested in not worked targets for period
                }
            );
        //console.log(rowsForReport);

        var mailBody = '';

        //Ordering report by Profit
        rowsForReport = _.sortBy(rowsForReport, function(row) {return row.profit;});

        if (rowsForReport.length){
            mailBody = '<table style="border: 1px solid black; border-collapse: collapse;" id="result"><tr><th>SRC</th><th>CMP</th><th>TGT</th><th>SPND</th><th>PFT</th><th>ROI</th><th>CPC</th><th>eCPC</th><th>tCPC</th></tr>';
            for (var rowIndex = 0; rowIndex<rowsForReport.length; rowIndex++){
                var row = rowsForReport[rowIndex];
                //traffic source, target, total spend today, loss today, ROI today, current CPC, effective CPC
                mailBody+='<tr><td align="left">'+row.source_abbr+'</td><td align="left">'+row.comp_name+'</td><td align="left">'+row.value+'</td><td align="center">'+htmlDigit(row.cost)+'$</td><td align="center">'+htmlDigit(row.profit)+'$</td><td align="center">'+htmlDigit(+row.roi*100)+'%</td><td align="center">'+htmlDigit(row.cpc)+'$</td><td align="center">'+htmlDigit(row.eCPC)+'$</td><td align="center">'+htmlDigit(row.tCPC)+'$</td></tr>';
            }
            mailBody+='</table>';
        }
        //console.log(mailBody);
        return {
                email: eml_recepient,
                dataObject: rowsForReport,
                dataHtml: mailBody,
                dataText: htmlToText.fromString(mailBody, {tables: ['#result']}),
                numberOfRows: rowsForReport.length,
                isMerge: 1,
                today: fromString+' - '+toString};
    }

    function htmlDigit(digit){
        return '<span'+(digit<0?' style="color:red; font-weight:bold"':'') +'>'+digit.toFixed(2)+'</span>';
    }

    //Main programm
    try {
        var formattedResult =
            getSettings().then(function(settings){
                //console.log(settings);
                var settingsObj = _.object(_.pluck(settings.data, 'name'), _.pluck(settings.data, 'value'));
                //console.log(settingsObj);

                marginLevel = +settingsObj.target_margin;

                //Notify report settings
                ROILimit = +settingsObj.ROILimit;
                LossLimit = +settingsObj.LossLimit;
                HideClicks = +settingsObj.HideClicks;

                //ROI report settings
                mergeReportROILossLimit = +settingsObj.MergeReportROILossLimit;
                mergeHours = +settingsObj.MergeReportHours;
                timeZone = settingsObj.thrive_time_zone_name;
                timeZoneShift = +settingsObj.thrive_time_zone_shift;

                //API Connectivity settings
                rootHost = settingsObj.thrive_root;

                apiKey = settingsObj.thrive_api_key;
                apiSecret = settingsObj.thrive_api_secret;
                apiParam = settingsObj.thrive_api_param;

                //Email sending settings
                eml_recepient = settingsObj.default_email;
                eml_copy_to = settingsObj.copy_to;
                eml_from = settingsObj.from;
                eml_sender = settingsObj.sender;
            }).then(getChannelsWithVaiables)
            .then(
            function(chData){
                //var sources = chData.sources;

                var result;
                if (isMergeReport){
                    //Merge report

                    var from = moment().subtract(mergeHours,'hours').utcOffset(timeZoneShift).format('MM/DD/YYYY');
                    var to = moment().utcOffset(timeZoneShift).format('MM/DD/YYYY');
                    result = getCampaignsWithMetrics(chData.camps, from, function(row) {
                        return row.roi<mergeReportROILossLimit;
                    }, to).then(function(report) {
                        return formatMergeReport(report, from, to);}
                    );
                }
                else{
                    //Notification report
                    var today = moment().utcOffset(timeZoneShift).format('MM/DD/YYYY');
                    result = getCampaignsWithMetrics(chData.camps, today, function(row) {
                        return (row.roi<ROILimit || row.profit<-LossLimit) &&
                                row.clicks > HideClicks; //We wil not show failure if target did not collected next HideClicks clicks
                    }).then(
                        function(camps){
                            return processNotifyReport(camps, today);
                        }
                    ).then(function(repRows){ return extendWithResponsible(repRows, chData.sources);})
                    .then(function(report) { return formatNotifyReport(report, today);});
                }
                return result;
            }
            )
        .then(function(formattedData){
            var result;
            if (_.isArray(formattedData)){
               var sendings = [];
               for (var i=0; i<formattedData.length; i++){
                   sendings.push(sendFormattedEmail(formattedData[i]));
               }
               result = promise.all(sendings);
            } else {
                result = sendFormattedEmail(formattedData);
            }
            return result;
        })['catch'](function(err){
            console.log(err.stack);
            console.log(JSON.stringify(err, null, null, '\t'));
            cb(err, null);
        });
        formattedResult.then(function(formattedData){
            cb(null, formattedData);
        });
    }
    catch (err) {
            console.log(err.stack);
            console.log(JSON.stringify(err, null, null, '\t'));
            cb(err, null);
    }
};

module.exports(ctx, function(err, result){
    if (err){
        console.error(err);
    }else{
        console.log(result);
    }
});