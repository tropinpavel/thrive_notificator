var ctx = {
    data: {
        ROILimit: '0.2',
        LossLimit: '50',
        HideClicks: '20',
        MergeReportROILossLimit: '0.1',
        MergeReport: false
    }
};

module.exports = function(context, cb) {
    //Dependencies
    var cfg = context.data;

    var request = require('request-promise');
    var querystring = require('querystring');
    var _ = require('underscore');
    var promise = require('bluebird');

    var htmlToText = require('html-to-text');

    //Uncomment when will use "official API"
    //var crypto = require('crypto');

    //Cookie storage for requests;
    var requestJar = request.jar();

    //Do we requesting a dayly merge report?
    var mergeReport = cfg.MergeReport;

    //Merge report ROI limit
    var mergeReportROILossLimit = +cfg.MergeReportROILossLimit;

    //Host name for using API
    var rootHost = 'http://tracktrack13.com';
    //API path extension for calling methods
    var apiRoot = '/ajax';

    //API path to drilldown report generation
    var apiReportGeneratePath = '/reports/generateReport';
    //API path to drilldown report fetching
    var apiReportGetPath ='/reports/getReportView';
    //Reports deletion endpoint
    var apiReportsDelete = '/reports/delete';
    //Secure cookie setter path
    var apiSecureLogin = '/user/secureLogin';

    //Traffic sources fetch endpoint
    //var apiGetSources = '/sources/get';


    //----------------Security critical-----------------
    //User login, who have acces to drilldown reports
    var consoleUsername = 'user@user.com';
    //Use password
    var consolePwd = 'dUBxydpI6svZATa';

    //API Settings
    var apiKey = '5244540dfa897c33b0df95f227f90138';
    var apiSecret = 'd99388eed66ecfac';
    var apiParam = '_0abe37c';

    //Stamplay API secrets
    var stamAPIAppID = 'thrive';
    var stamAPIAppSecret = '8084091ecd27a466ae7dbd0d34a152c7b2fa6d01c1b11438d9ee69e11aee4f44';
    //--------------------------------------------------

    //---------------Settings---------------------------
    var ROILimit = +cfg.ROILimit;
    var LossLimit = +cfg.LossLimit;
    var HideClicks = +cfg.HideClicks;
    //--------------------------------------------------

    //Stampay application url
    var stamUrl = 'http://'+stamAPIAppID+'.stamplayapp.com/api/cobject/v1';
    var stamObject = '/outsiders';

    //Default options for report generation
    var d_generateReportOptions = {
        range: {
            timezone: 'US/Eastern' //Timezone to be used in generating report
        },
        keys:[ 'custom.target'] //Report grouping criteria
    };

    //Default options for reports fetching
    var d_getReportOptions = {
        //tableNumber: 0,
        sortBy: 'profit', //Sorting field. We need to sort by profit to get unprofitable first
        direction: 'ASC', //Sorting direction
        rowStart: 0, //Get report from very first line
        rowEnd: -1 //Get all report lines
    };

    //Get hash for API calls
    /*
    function makeApiHash(key, secret, offset) {
        offset = offset || 0;
        if(typeof key !== 'string' || key.length != 32) {
            return false;
        }
        if(typeof secret !== 'string' || secret.length != 16) {
            return false;
        }
        var now = Math.floor(new Date() / 1000) + (offset * 60);
        // hash changes every 60 seconds
        var date = Math.round(Math.ceil((now / 60)) * 60);

        var md5 = crypto.createHash('md5').update(key + secret + date).digest("hex");
        var sha256 = crypto.createHash('sha256').update(md5).digest('hex');
        return sha256;
    }

    function getChannelsWithVaiables(){
        var apiHash = makeApiHash(apiKey, apiSecret);
        if (apiHash){
            var r_params = {};
            r_params[apiParam] = apiHash;
            request({ uri: rootHost+apiRoot+apiGetSources, method: 'POST', form: r_params}).then(
                function(body){
                    var data = JSON.parse(body);
                    console.log(JSON.stringify(data, null, '\t'));
                }
            )
        }
    }*/

    //Adding record to storage about outdider
    function addRowToStorage(row){
        return request({
            url: stamUrl+stamObject,
            method: 'POST',
            json: {
                clicks: row.clicks,
                cost: row.cost,
                cpc: row.cpc,
                ecpc: row.eCPC,
                key: row.gKey,
                plus_clicks: row.plus_clicks,
                profit: row.profit,
                roi: row.roi,
                target: row.value,
                r_date: row.r_date
            },
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            }
        });
    }

    //Update outsider info
    function updateRowInStorage(id, row){
        return request({
            url: stamUrl+stamObject+'/'+id,
            method: 'PUT',
            json: {
                clicks: row.clicks,
                cost: row.cost,
                cpc: row.cpc,
                ecpc: row.eCPC,
                key: row.gKey,
                plus_clicks: row.plus_clicks,
                profit: row.profit,
                roi: row.roi,
                target: row.value,
                r_date: row.r_date
            },
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            }
        });
    }

    //Get already saved list of outsiders
    function getOutsiders(keys, dateString){
        var whereClousure = {
            "$and":[
            {"key": {"$in": keys}},
            { "r_date": {"$eq": dateString}}
        ]};

        return request({
            url: stamUrl+stamObject,
            method: 'GET',
            qs: {
                where: JSON.stringify(whereClousure)
            },
            'auth': {
                'user': stamAPIAppID,
                'pass': stamAPIAppSecret
            }
        });
    }

    //Set auth cookie for all future requests that wiil use requestJar
    function authWebApiRequests(){
        return request({ uri: rootHost, method: 'GET', jar: requestJar}).then(function(body){
            //console.log(body);
            var nextRequest;
            //Get login endpoint to get JSONP login token
            var searchLogin = /'(https:\/\/accounts.ipyxel.com\/thrive\/process-functions\/jsonp\/v2\/user\/checkLogin2.*)'/gim;
            searchLogin = searchLogin.exec(body);
            if (searchLogin.length>1){ //Endpoint found
                //Forming final request string to get token
                searchLogin = searchLogin[1] + '?' +
                    querystring.stringify({
                        user: consoleUsername.toLowerCase(),
                        pass: consolePwd,
                        callback: 'CB',
                        _: String(Math.random()).slice(-6)
                    });
                //console.log(searchLogin);
                //Fetching the token
                nextRequest = request({ uri: searchLogin, method: 'GET', jar: requestJar});
            } else {
                console.log(body);
                throw new Error('Cant find login url for token request');
            }
            return nextRequest;
        }).then(function(body){
            var nextRequest;
            //We got token
            //And need to extract it from JSONP result
            var searchResp = /CB\((.*)\)/gi;
            searchResp = searchResp.exec(body);
            if (searchResp.length>1){
                var token = JSON.parse(searchResp[1]).token;
                if (token){
                    //console.log(token);
                    nextRequest = request({ uri: rootHost+apiRoot+apiSecureLogin, method: 'POST', form: {token:token, remember: ''}, jar: requestJar});
                } else {
                    console.log(body);
                    throw new Error('Incorrect login token recieved');
                }
            } else {
                console.log(body);
                throw new Error('Cant find token in login responce');
            }
            return nextRequest;
        }).then(function(body){
                //console.log(requestJar.getCookieString(rootHost));
                var nextRequest = false;
                //We have secure cookies set, now we can generate report
                if (!JSON.parse(body).error){
                    nextRequest = true;
                } else {
                    console.log(body);
                    throw new Error('Cant set secure cookie to perform future requests');
                }
                return nextRequest;
            }
        );
    }

    function generateAndGetReport(generateDay){
        var generateReportOptions = _.clone(d_generateReportOptions);
        var getReportOptions = _.clone(d_getReportOptions);

        //Setting report generation dates range (limit on current day in selected previousely timezone)
        generateReportOptions.range.from =  generateReportOptions.range.to = generateDay;
        //Generating report
        return request({ uri: rootHost+apiRoot+apiReportGeneratePath, method: 'POST', form: generateReportOptions, jar: requestJar}).then(
            function(body){
                var nextRequest;
                //We just generated a report
                var apiResult = JSON.parse(body);
                if (!apiResult.error){
                    //console.log(apiResult.report);
                    //Setting Id of report just generated and setting to get it in next step
                    getReportOptions.reportId = apiResult.report.id;
                    //console.log(apiResult.report.id);
                    nextRequest =  request({ uri: rootHost+apiRoot+apiReportGetPath, method: 'POST', form: getReportOptions, jar: requestJar});
                } else{
                    console.log(body);
                    throw new Error('Cant generate report '+ apiResult.message);
                }
                return nextRequest;
            }).then(function(body){
                //We got a freshly generated report contents
                var result = [];
                var apiResult = JSON.parse(body);
                if (!apiResult.error && apiResult.data){
                    //Send command to delete report
                    result = request({ uri: rootHost+apiRoot+apiReportsDelete, method: 'POST', form: {reports: [ getReportOptions.reportId ]}, jar: requestJar}).then(
                        function(data){
                            var res = JSON.parse(data);
                            if (!res.error){
                                //console.log('Report Id ='+getReportOptions.reportId+' was deleted');
                            }else{
                                console.log('Report Id ='+getReportOptions.reportId+' was not deleted :' + res.message);
                            }
                            //Setting report data
                            return apiResult.data;
                        });
                } else if (apiResult.error){
                    throw  new Error('Cant get report with id = '+getReportOptions.reportId+' : '+body);
                }
                return result;
            });
    }


    try {
        //When API will support reporting, than we will use an "official" way
        authWebApiRequests().then(
            function(authResult){
                var result = false;
                if (authResult){
                    var dateNow = new Date();
                    var todayString =  (dateNow.getMonth() + 1) + "/" + dateNow.getDate() + "/" + dateNow.getFullYear();
                    if (mergeReport){//Merge report
                        var yesterDate = new Date();
                        yesterDate.setDate(yesterDate.getDate() - 1);
                        var yesterdayString =  (yesterDate.getMonth() + 1) + "/" + yesterDate.getDate() + "/" + yesterDate.getFullYear();
                        result = generateAndGetReport(yesterdayString).then(function(yesterdayReport){
                            return generateAndGetReport(todayString).then(function(todayReport){
                                /*
                                console.log('yesteday: '+yesterdayString);
                                console.log(yesterdayReport);
                                console.log('today: '+todayString);
                                console.log(todayReport);*/
                                return mergeReports(yesterdayReport, todayReport, yesterdayString, todayString);
                            });
                        });
                    } else { //Immediate report
                        result = generateAndGetReport(todayString).then(
                            function(reportData){
                                return processReport(reportData, todayString);
                            }
                        );
                    }
                }
                return result;
        }).then(function(mailPayload){
            cb(null, mailPayload);
        })['catch'](function(err){
            console.log(err.stack);
            console.log(JSON.stringify(err, null, null, '\t'));
            cb(err, null);
        });
    }
    catch (err) {
        console.log(err.stack);
        cb(err, null);
    }

    //Report processing routine
    function processReport(reportData, todayString){
        //Our report already sorted by profit ASC (that means that most unprofitable rows will be at top)
        var currentRowsForReport = [];
        for (var rowIndex = 0; rowIndex<reportData.length; rowIndex++){
            var row = reportData[rowIndex];
            row.eCPC = row.rev/row.clicks;
            if (
                (row.roi<ROILimit || row.profit<-LossLimit) &&
                    row.clicks > HideClicks //We wil not show failure if target did not collected next HideClicks clicks
            ){
                currentRowsForReport.push(row);
            }
        }

        //Need to search for already saved ousiders (history from previous runs)
        return getOutsiders( _.pluck(currentRowsForReport, 'gKey'), todayString).then(
            function(body){
                //console.log(body);
                var savedOutsiders = JSON.parse(body).data;
                var rowsForReport = [];
                var storageOps = [];

                //console.log(currentRowsForReport);
                //console.log(savedOutsiders);
                for(var i = 0; i<currentRowsForReport.length; i++){
                    var newItem = currentRowsForReport[i];
                    var savedItem = _.findWhere(savedOutsiders, {key: newItem.gKey});
                    newItem.r_date = todayString;
                    newItem.plus_clicks = savedItem?newItem.clicks-savedItem.clicks:0;
                    //console.log(savedItem);
                    if (savedItem){ //Maybe we need to update saved one
                       if (newItem.plus_clicks>HideClicks){ //Still working/clicking and wasting money
                           rowsForReport.push(newItem);
                           storageOps.push(updateRowInStorage(savedItem._id, newItem));
                       }
                    } else {
                        rowsForReport.push(newItem);
                        storageOps.push(addRowToStorage(newItem));
                    }
                }
                return promise.all(storageOps).then(function(){
                    //Target, total spend today, loss today, ROI today, current CPC, effective CPC
                    var mailBody = '';
                    if (rowsForReport.length){
                        mailBody = '<table id="result"><tr><th>Target</th><th>Total spend today</th><th>Profit today</th><th>ROI today</th><th>Current CPC</th><th>Current effective CPC</th><th>New clicks from last run!</th></tr>';
                        for (rowIndex = 0; rowIndex<rowsForReport.length; rowIndex++){
                            var row = rowsForReport[rowIndex];
                            //traffic source, target, total spend today, loss today, ROI today, current CPC, effective CPC
                            mailBody+='<tr><td align="left">'+row.value+'</td><td align="center">'+row.cost+'$</td><td align="center">'+row.profit+'$</td><td align="center">'+(+row.roi*100)+'%</td><td align="center">'+row.cpc+'$</td><td align="center">'+row.eCPC+'$</td><td align="center">+'+row.plus_clicks+'clk</td></tr>';
                        }
                        mailBody+='</table>';
                    }
                    return {dataTable: htmlToText.fromString(mailBody, {tables: ['#result']}), numberOfRows: rowsForReport.length, isMerge: 0, today: todayString};
                });
            }
        );
    }

    //Getting only lines that have bad distance in roi
    function mergeReports(yesterdayReport, todayReport, yesterdayString, todayString){
        var roiLosses = [];
        var largeReport = yesterdayReport.length>todayReport.length? yesterdayReport : todayReport;
        var smallReport = yesterdayReport.length>todayReport.length? todayReport : yesterdayReport;
        var prevFlag = largeReport === yesterdayReport;
        //Getting roi loses
        for (var i = 0; i<largeReport.length; i++){
            var firstRow = largeReport[i];
            var secondRow =  _.findWhere(smallReport, {gKey: firstRow.gKey});
            if (secondRow){ //We have sub-channel in both reports
                var oldValue = prevFlag? firstRow: secondRow;
                var newValue = prevFlag? secondRow: firstRow;
                var daylyROIdiff = newValue.roi-oldValue.roi;
                //console.log(daylyROIdiff);
                if (daylyROIdiff<-mergeReportROILossLimit){
                    roiLosses.push({
                        target: oldValue.value,
                        roiYesteday: oldValue.roi,
                        roiToday: newValue.roi,
                        roiDiff: daylyROIdiff
                    });
                }
            }
        }
        //Printing email body
        var mailBody = '<table id="result"><tr><th>Target</th><th>ROI Yesterday '+yesterdayString+'</th><th>ROI Today '+todayString+'</th><th>ROI Loss</th></tr>';
        for (i = 0; i<roiLosses.length; i++){
            var row = roiLosses[i];
            mailBody+='<tr><td align="left">'+row.target+'</td><td align="center">'+(row.roiYesteday*100)+'%</td><td align="center">'+(row.roiToday*100)+'%</td><td align="center">'+(row.roiDiff*100)+'%</td></tr>';
        }
        mailBody+='</table>';
        return {dataTable: htmlToText.fromString(mailBody, {tables: ['#result']}), numberOfRows: roiLosses.length, isMerge: 1, today: todayString};
    }
};

module.exports(ctx, function(err, result){
    if (err){
        console.error(err);
    }else{
        console.log(result);
    }
})